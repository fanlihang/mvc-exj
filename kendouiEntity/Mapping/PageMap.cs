﻿using Core;
using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kendouiEntity.Mapping
{
    public class PageMap : EntityTypeConfiguration<Page>
    {
        public PageMap()
        {
            HasRequired<Domain.Type>(x => x.Type).WithMany(x => x.Pages).Map(
                x => x.MapKey("TypeID"));
            HasMany<PageContent>(x => x.PageContents).WithRequired(x => x.Page)
                .Map(x => x.MapKey("PageID")).WillCascadeOnDelete();
            HasMany<Tag>(x => x.PageTags).WithMany(x => x.Pages).Map(m =>
            {
                m.ToTable("PageTags");
                m.MapLeftKey("PageID");
                m.MapRightKey("TagID");
            });
            HasMany<PageComment>(x => x.PageComment).WithRequired(x => x.Page);
            this.ToTable("Page");
        }
    }

    public class CommentMap : EntityTypeConfiguration<Domain.Comment>
    {
        public CommentMap()
        {
            HasRequired<User>(x => x.CommentedUser);
            ToTable("Comment");

        }
    }

    public class CommentReplyMap : EntityTypeConfiguration<CommentReply>
    {
        public CommentReplyMap()
        {
            HasRequired<Comment>(x => x.Comment).WithMany(y => y.CommentReplyList);
            HasOptional<User>(x => x.To);
            ToTable("CommentReply");
        }
    }

    public class PageCommentMap : EntityTypeConfiguration<PageComment>
    {
        public PageCommentMap()
        {
            ToTable("PageComment");
        }
    }

    public class TypeMap : EntityTypeConfiguration<Domain.Type>
    {
        public TypeMap()
        {
            this.ToTable("Type");
        }
    }

    public class TagMap : EntityTypeConfiguration<Tag>
    {
        public TagMap()
        {
            this.ToTable("Tag");
        }
    }

    public class PageContentMap : EntityTypeConfiguration<PageContent>
    {
        public PageContentMap()
        {
            this.ToTable("PageContent");
        }
    }

    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            this.ToTable("User");
        }
    }
}
