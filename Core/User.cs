﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class User : BaseModel
    {
        /// <summary>
        /// 帐号
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public virtual string Password { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string Name { get; set; }


        /// <summary>
        /// QQ号
        /// </summary>
        public virtual string QQ { get; set; }

        /// <summary>
        /// Msn
        /// </summary>
        public virtual string Msn { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public virtual string PhoneNum { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTime CreationDate { get; set; }

    }
}
