﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{

    /// <summary>
    /// 页面内容
    /// </summary>
    public class Page : BaseModel
    {
        /// <summary>
        /// 前置图片
        /// </summary>
        public string ImageLogo { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 小标题
        /// </summary>
        public string TitleLevel2 { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// 来源 
        /// </summary>
        public string From { get; set; }
        /// <summary>
        /// 最小内容，即引言
        /// </summary>
        public string MinContent { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 文章页面长度
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 是否为置顶
        /// </summary>
        public bool IsTop { get; set; }
        /// <summary>
        /// 是否为火热
        /// </summary>
        public bool IsHot { get; set; }
        /// <summary>
        /// 文章所含标签
        /// </summary>
        public List<Tag> PageTags { get; set; }
        /// <summary>
        /// 文章所含主体内容
        /// </summary>
        public List<PageContent> PageContents { get; set; }
        /// <summary>
        /// 文章所在分类
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// 文章的评论
        /// </summary>
        public List<PageComment> PageComment { get; set; }
    }

    /// <summary>
    /// 主体内容
    /// </summary>
    public class PageContent : BaseModel
    {
        /// <summary>
        /// 主体内容的具体
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 主体内容的序号(必须为连续的)
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 主体内容所属的文章
        /// </summary>
        public Page Page { get; set; }
    }

    /// <summary>
    /// 分类节点
    /// </summary>
    public class Type : BaseModel
    {
        /// <summary>
        /// 分类的名称
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 分类在树中的节点等级从1级开始
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// 分类在树中所经过的路径（请同步更新此属性）
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 分类的上一个树的节点ID
        /// </summary>
        public int ParentID { get; set; }
        /// <summary>
        /// 分类下所含有的文章(非必须)
        /// </summary>
        public List<Page> Pages { get; set; }
    }

    /// <summary>
    /// 标签
    /// </summary>
    public class Tag : BaseModel
    {
        /// <summary>
        /// 标签的名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 标签的权重
        /// </summary>
        public int PR { get; set; }
        /// <summary>
        /// 含有此标签的文章列表
        /// </summary>
        public List<Page> Pages { get; set; }
    }

    /// <summary>
    /// 评论
    /// </summary>
    public class Comment : BaseModel
    {
        /// <summary>
        /// 评论者
        /// </summary>
        public User CommentedUser { get; set; }
        /// <summary>
        /// 评论的内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 评论的时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 此人所评论后得到的回复
        /// </summary>
        public List<CommentReply> CommentReplyList { get; set; }
    }

    /// <summary>
    /// 评论的回复(继续自评论，可实现为树型结果的楼中楼)
    /// </summary>
    public class CommentReply : Comment
    {
        /// <summary>
        /// 回复的对象
        /// </summary>
        public Comment Comment { get; set; }
        /// <summary>
        /// 回复给谁（默认为空即为回复给评论者，否则即为回复给评论下的回复者）
        /// </summary>
        public User To { get; set; }
    }

    /// <summary>
    /// 文章的评论
    /// </summary>
    public class PageComment : Comment
    {
        /// <summary>
        /// 文章的ID
        /// </summary>
        public Page Page { get; set; }
    }

}
