﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using kendouiEntity;
using Domain;

namespace Test
{
    [TestClass]
    public class DbTest
    {
        [TestMethod]
        public void CreateDb()
        {
            var dbContent = new ScpObjectContext();
            if (!dbContent.Database.Exists())
            {
                dbContent.Database.Create();
            }
            else
            {
                dbContent.Database.Delete();
                dbContent.Database.Create();
            }

        }

        [TestMethod]
        public void InsertDb()
        {
            var dbContent = new ScpObjectContext();

            IRepository<Domain.Type> typeRepository = new EFRepository<Domain.Type>(dbContent);

            var tempType = from t in typeRepository.Table
                           where t.TypeName == "文章"
                           select t;
            Domain.Type type = new Domain.Type();
            if (tempType.FirstOrDefault() == null)
            {
                type.Level = 0;
                type.ParentID = 0;
                type.TypeName = "文章";
                type.Path = "0";
                typeRepository.Insert(type);
            }

            var tempTypeEntity = tempType.FirstOrDefault();
            if (tempTypeEntity != null)
            {
                type.Level = 1;
                type.ParentID = tempTypeEntity.Id;
                type.TypeName = "Dota";
                type.Path = "0_1";
                typeRepository.Insert(type);
            }
        }

        [TestMethod]
        public void InsertPage()
        {
            var dbContent = new ScpObjectContext();

            Domain.Page Page = new Domain.Page();
            IRepository<Domain.Page> pageRepository = new EFRepository<Domain.Page>(dbContent);
            IRepository<Domain.Type> typeRepository = new EFRepository<Domain.Type>(dbContent);
            var typeQ = from t in typeRepository.Table
                        where t.TypeName == "Dota"
                        select t;
            if (typeQ.FirstOrDefault() == null)
            {
                InsertDb();
            }
            var type = typeQ.FirstOrDefault();

            Page.Type = type;
            Page.Title = "Dota 传说哥!!!";
            Page.TitleLevel2 = "Dota 传说哥";
            Page.Author = "小小强";
            Page.From = "传说";
            Page.ImageLogo = "www";
            Page.IsHot = true;
            Page.IsTop = true;
            Page.PageSize = 1;
            Page.CreateTime = DateTime.Now;

            pageRepository.Insert(Page);
            PageContent pc = new PageContent()
            {
                Content = "这是第一页的内容",
                Page = Page,
                PageIndex = 1
            };
            IRepository<Domain.PageContent> pcrp = new EFRepository<Domain.PageContent>(dbContent);
            pcrp.Insert(pc);
        }

        [TestMethod]
        public void GetPage()
        {
            IDbContext dbcontent = new ScpObjectContext();
            IRepository<Page> prp = new EFRepository<Page>(dbcontent);
            var page = prp.GetById(8);
            if(page!=null){
                Console.Write(page.ToString());
            }
        }
    }
}
