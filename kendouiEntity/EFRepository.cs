﻿using Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kendouiEntity
{
    public class EFRepository<T> : IRepository<T> where T : BaseModel
    {
        private readonly IDbContext _context;
        private IDbSet<T> _entities;

        public EFRepository(IDbContext context)
        {
            this._context = context;
        }

        private void RunEF(Action<T> fun, T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                fun(entity);
                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        msg += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);

                var fail = new Exception(msg, dbEx);
                //Debug.WriteLine(fail.Message, fail);
                throw fail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Insert(T entity)
        {
            RunEF(x => { this.Entities.Add(x); }, entity);
        }

        public void Update(T entity)
        {
            RunEF(x => { ; }, entity);
        }

        public void Delete(T entity)
        {
            RunEF(x => { this.Entities.Remove(entity); }, entity);
        }

        public IQueryable<T> Table
        {
            get { return this.Entities; }
        }

        public T GetById(object id)
        {
            return this.Entities.Find(id);
        }

        private IDbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _context.Set<T>();
                return _entities;
            }
        }
    }
}
